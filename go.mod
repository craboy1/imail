module github.com/midoks/imail

go 1.16

require (
	github.com/axgle/mahonia v0.0.0-20180208002826-3358181d7394
	github.com/editorconfig/editorconfig-core-go/v2 v2.4.3
	github.com/go-macaron/binding v1.1.1
	github.com/go-macaron/cache v0.0.0-20200329073519-53bb48172687
	github.com/go-macaron/captcha v0.2.0
	github.com/go-macaron/csrf v0.0.0-20200329073418-5d38f39de352
	github.com/go-macaron/gzip v0.0.0-20200329073552-98214d7a897e
	github.com/go-macaron/i18n v0.6.0
	github.com/go-macaron/session v1.0.2
	github.com/go-resty/resty/v2 v2.6.0
	github.com/jarcoal/httpmock v1.0.8
	github.com/jonboulle/clockwork v0.2.2 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.5 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/microcosm-cc/bluemonday v1.0.15
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/rifflock/lfshook v0.0.0-20180920164130-b9218ef580f5
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli v1.22.5
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	golang.org/x/sys v0.0.0-20210630005230-0f9fa26af87c // indirect
	golang.org/x/text v0.3.7
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/ini.v1 v1.63.2
	gopkg.in/macaron.v1 v1.4.0
	gorm.io/driver/mysql v1.1.2
	gorm.io/driver/sqlite v1.1.5
	gorm.io/gorm v1.21.15
)
